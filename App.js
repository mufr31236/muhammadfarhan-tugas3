import {View, Text} from 'react-native';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from './src/HomeScreen';
import NetinfoScreen from './src/NetinfoScreen';
import DatePicker from './src/DatePicker';
import SliderScreen from './src/SliderScreen';
import GeoLocationScreen from './src/GeoLocation';
import ProgressScreen from './src/ProgressScreen';
import ProgressBarScreen from './src/ProgressBarScreen';
import ClipBoardScreen from './src/ClipBoardScreen';
import AsyncStorageScreen from './src/AsyncStorageScreen';
import RenderHtmlScreen from './src/RenderHtmlScreen';
import SkletonPlcaeholderScreen from './src/SkletonPlcaeholderScreen';
import WebviewScreen from './src/WebviewScreen';
import LinearGradientScreen from './src/LinearGradientScreen';
import ShareScreen from './src/ShareScreen';
import CarouselScreen from './src/CarouselScreen';
import {Share} from 'react-native';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="home" component={HomeScreen} />
        <Stack.Screen name="netinfo" component={NetinfoScreen} />
        <Stack.Screen name="datepicker" component={DatePicker} />
        <Stack.Screen name="slider" component={SliderScreen} />
        <Stack.Screen name="geolocation" component={GeoLocationScreen} />
        <Stack.Screen name="progress" component={ProgressScreen} />
        <Stack.Screen name="progressbar" component={ProgressBarScreen} />
        <Stack.Screen name="clipboard" component={ClipBoardScreen} />
        <Stack.Screen name="storage" component={AsyncStorageScreen} />
        <Stack.Screen name="html" component={RenderHtmlScreen} />
        <Stack.Screen name="skleton" component={SkletonPlcaeholderScreen} />
        <Stack.Screen name="webview" component={WebviewScreen} />
        <Stack.Screen name="gradient" component={LinearGradientScreen} />
        <Stack.Screen name="share" component={ShareScreen} />
        <Stack.Screen name="carousel" component={CarouselScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
