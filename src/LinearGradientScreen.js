import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';

const LinearGradientScreen = () => {
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={['red', 'yellow', 'green']}
        style={styles.linearGradient}>
        <Text>Vertical Gradient</Text>
      </LinearGradient>
    </View>
  );
};

export default LinearGradientScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  linearGradient: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: 200,
    width: 350,
  },
});
