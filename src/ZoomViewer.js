import {
  StyleSheet,
  Text,
  View,
  Modal,
  TouchableOpacity,
  Image,
  Button,
  Touchable,
} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import Icon from 'react-native-vector-icons/FontAwesome5';
import React from 'react';

const walpaper = [
  {
    props: {
      source: require('./assets/walpaper1.jpg'),
    },
  },
  {
    props: {
      source: require('./assets/walpaper2.jpg'),
    },
  },
  {
    props: {
      source: require('./assets/walpaper3.jpg'),
    },
  },
];

export default function ZoomViewer({show, onClose}) {
  return (
    <Modal visible={show} transparent={true}>
      <View style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
        <View style={{alignSelf: 'stretch', backgroundColor: 'black'}}>
          <Icon
            name="times"
            size={30}
            color="white"
            onPress={onClose}
            style={{
              backgroundColor: 'black',
              alignSelf: 'flex-start',
              paddingHorizontal: 10,
              paddingVertical: 10,
            }}
          />
        </View>

        <ImageViewer imageUrls={walpaper} />
      </View>
    </Modal>
  );
}
const styles = StyleSheet.create({});
