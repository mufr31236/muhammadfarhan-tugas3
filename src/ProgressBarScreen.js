import {View, Text} from 'react-native';
import React from 'react';
import {StyleSheet} from 'react-native';
import {ProgressBar} from '@react-native-community/progress-bar-android';

const ProgressBarScreen = () => {
  return (
    <View style={style.container}>
      <Text style={style.title}>Progress Bar Screen</Text>
      <ProgressBar style={style.bar} />
      <Text style={{alignSelf: 'center', marginVertical: 10}}>
        Waiting for other player...
      </Text>
    </View>
  );
};

export default ProgressBarScreen;

const style = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    padding: 10,
  },

  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 10,
    marginHorizontal: 10,
    alignSelf: 'center',
  },

  bar: {
    marginVertical: 24,
    height: 80,
  },
});
