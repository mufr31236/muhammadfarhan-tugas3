import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import {ProgressView} from '@react-native-community/progress-view';

const ProgressScreen = () => {
  const [progress, setProgress] = useState(0);
  return (
    <View style={style.container}>
      <Text style={style.title}>Progress Screen</Text>
      <ProgressView
        progressTintColor="orange"
        trackTintColor="blue"
        progress={progress}
      />
      <TouchableOpacity
        style={style.button}
        onPress={() => setProgress(progress + 0.1)}>
        <Text style={style.buttonText}>Push to progress</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={style.button}
        onPress={() => setProgress(progress - 0.1)}>
        <Text style={style.buttonText}>Push to decrease</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ProgressScreen;

const style = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    padding: 10,
  },

  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 10,
    marginHorizontal: 10,
    alignSelf: 'center',
  },

  rateValue: {
    fontSize: 16,
    fontWeight: 'bold',
    marginVertical: 10,
    marginHorizontal: 10,
    alignSelf: 'center',
    borderColor: 'black',
    borderRadius: 1,
  },

  button: {
    marginHorizontal: 30,
    marginVertical: 20,
    backgroundColor: '#2E3283',
    alignSelf: 'stretch',
    borderRadius: 10,
    alignContent: 'center',
  },

  buttonText: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
});
