import {View, Text, StyleSheet} from 'react-native';
import React, {useState} from 'react';
import {TextInput} from 'react-native';
import {TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const AsyncStorageScreen = () => {
  const [inputText, setInputText] = useState('');
  const [savedText, setSavedText] = useState('');

  const saveData = async text => {
    try {
      await AsyncStorage.setItem('my-key', text);
    } catch (e) {
      console.log(e);
    }
  };

  const getData = async () => {
    try {
      const text = await AsyncStorage.getItem('my-key');
      if (text !== null) {
        setSavedText(text);
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.inputText}
        placeholder="Input text to save here"
        value={inputText}
        onChangeText={text => setInputText(text)}
      />
      <TouchableOpacity
        style={styles.button}
        onPress={() => saveData(inputText)}>
        <Text style={styles.buttonText}>Save</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          getData();
          console.log('this is saved text:' + savedText);
        }}>
        <Text style={styles.buttonText}>Get Saved Text</Text>
      </TouchableOpacity>

      <Text style={styles.copiedText}>{savedText}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  inputText: {
    marginVertical: 10,
    alignSelf: 'stretch',
    marginHorizontal: 20,
  },

  copiedText: {
    marginTop: 10,
    color: 'red',
  },

  button: {
    marginHorizontal: 30,
    marginVertical: 20,
    backgroundColor: '#2E3283',
    alignSelf: 'stretch',
    borderRadius: 10,
    alignContent: 'center',
  },

  buttonText: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default AsyncStorageScreen;
