import {View, Text, StyleSheet} from 'react-native';
import React, {useState} from 'react';
import Slider from '@react-native-community/slider';

const SliderScreen = () => {
  const [slideVal, setSlideVal] = useState(0);
  return (
    <View style={style.container}>
      <Text style={style.title}>Rate Us</Text>
      <Slider
        style={{width: 200, height: 40, alignSelf: 'center'}}
        minimumValue={0}
        maximumValue={1}
        minimumTrackTintColor="#FFFFFF"
        maximumTrackTintColor="#000000"
        onValueChange={num => setSlideVal(num)}
      />
      <Text style={style.rateValue}>{slideVal}</Text>
    </View>
  );
};

export default SliderScreen;

const style = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    padding: 10,
  },

  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 10,
    marginHorizontal: 10,
    alignSelf: 'center',
  },

  rateValue: {
    fontSize: 16,
    fontWeight: 'bold',
    marginVertical: 10,
    marginHorizontal: 10,
    alignSelf: 'center',
    borderColor: 'black',
    borderRadius: 1,
  },
});
