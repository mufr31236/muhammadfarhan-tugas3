import {View, Text} from 'react-native';
import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import Tooltip from 'rn-tooltip';
import {Button} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ZoomViewer from './ZoomViewer';
import Share from 'react-native-share';
import {Linking} from 'react-native';

const HomeScreen = ({navigation}) => {
  const [zoomViewer, setZoomViewer] = useState(false);

  const shareOptions = {
    title: 'Share via',
    message: 'some Beli buku',
    // url: 'some share url',
    social: Share.Social.SMS,
    // whatsAppNumber: '9199999999', // country code + phone number
    recipient: '6281364440815',
    // filename: 'test', // only for base64 file in Android
  };

  const onShare = async () => {
    try {
      await Share.shareSingle(shareOptions);
    } catch (error) {
      console.log(error);
    }
    // Linking.openURL('whatsapp.com/62');
  };

  return (
    <View style={style.container}>
      <ScrollView>
        <ZoomViewer show={zoomViewer} onClose={() => setZoomViewer(false)} />
        <Text style={style.title}>React Native Library</Text>
        <Icon
          style={{alignSelf: 'center'}}
          name="rocket"
          size={30}
          color="#900"
        />
        <Tooltip popover={<Text>Rn tooltip example</Text>}>
          <Text
            style={{
              backgroundColor: 'black',
              color: 'white',
              margin: 10,
              alignSelf: 'center',
              padding: 10,
            }}>
            more info
          </Text>
        </Tooltip>
        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('netinfo')}>
          <Text style={style.buttonText}>Netinfo Screen</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('datepicker')}>
          <Text style={style.buttonText}>Date Picker Screen</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('slider')}>
          <Text style={style.buttonText}>Slider Screen</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('geolocation')}>
          <Text style={style.buttonText}>GeoLocation</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('progress')}>
          <Text style={style.buttonText}>Progress Screen</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('progressbar')}>
          <Text style={style.buttonText}>Progress Bar Screen</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('clipboard')}>
          <Text style={style.buttonText}>Clipboard Screen</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('storage')}>
          <Text style={style.buttonText}>Storage Screen</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('html')}>
          <Text style={style.buttonText}>Html Screen</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('skleton')}>
          <Text style={style.buttonText}>Skleton Screen</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('webview')}>
          <Text style={style.buttonText}>Webview Screen</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('gradient')}>
          <Text style={style.buttonText}>Gradient Screen</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => setZoomViewer(true)}>
          <Text style={style.buttonText}>Zoom Viewer</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={style.button}
          onPress={() => navigation.navigate('carousel')}>
          <Text style={style.buttonText}>Carousel Screen</Text>
        </TouchableOpacity>

        <TouchableOpacity style={style.button} onPress={onShare}>
          <Text style={style.buttonText}>Share</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default HomeScreen;

const style = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },

  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 10,
    marginHorizontal: 10,
    alignSelf: 'center',
  },

  button: {
    marginHorizontal: 30,
    marginVertical: 10,
    backgroundColor: '#2E3283',
    alignSelf: 'stretch',
    borderRadius: 10,
    alignContent: 'center',
  },

  buttonText: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
});
