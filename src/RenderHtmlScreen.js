import {View, Text} from 'react-native';
import React from 'react';
import {useWindowDimensions} from 'react-native';
import RenderHTML from 'react-native-render-html';

const htmlPage = {
  html: `
    <p style='text-align:center;'>
      This from html
    </p>
    <a style='align-self:center;' href="https://www.youtube.com/watch?v=PzqQSOaCcnw">Click me</a>
    `,
};

const RenderHtmlScreen = () => {
  const {width} = useWindowDimensions();
  return <RenderHTML contentWidth={width} source={htmlPage} />;
};

export default RenderHtmlScreen;
