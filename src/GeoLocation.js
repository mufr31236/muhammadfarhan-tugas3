import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  PermissionsAndroid,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Geolocation from '@react-native-community/geolocation';

const GeoLocationScreen = () => {
  const [latitude, setLatitude] = useState('');
  const [longitude, setLongitude] = useState('');
  const [status, setStatus] = useState('');

  const getLocation = () => {
    Geolocation.getCurrentPosition(
      position => {
        const currentLongt = JSON.stringify(position.coords.longitude);
        const currentLat = JSON.stringify(position.coords.latitude);
        setLatitude(currentLat);
        setLongitude(currentLongt);
      },
      error => {
        setStatus(error.message);
      },
      {
        enableHighAccuracy: false,
        maximumAge: 1000,
      },
    );
  };
  return (
    <View style={styles.screen}>
      <View style={styles.content}>
        <Text style={styles.title}>Your Position</Text>
        <Text style={styles.textContent}>Latitude : {latitude}</Text>
        <Text style={styles.textContent}>Longitude : {longitude}</Text>
        <TouchableOpacity style={styles.btnGetPosition} onPress={getLocation}>
          <Text style={styles.txtButton}>Get Position</Text>
        </TouchableOpacity>
        <Text>{status}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 30,
  },
  content: {
    alignItems: 'center',
  },
  title: {
    fontSize: 40,
    color: 'black',
    fontWeight: 'bold',
    marginBottom: 20,
  },
  textContent: {
    fontSize: 20,
    color: 'black',
    marginBottom: 10,
  },
  btnGetPosition: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 10,
  },
  txtButton: {
    color: 'white',
  },
});

export default GeoLocationScreen;
